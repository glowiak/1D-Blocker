<p align="center">
	<img src="http://codeberg.org/glowiak/1d-blocker/raw/branch/master/app/src/main/resources/textures/logo_text.png">
</p>

# 1D-Blocker

A Minecraft-inspired RPG game, but in 1 Dimension.

### Installing

It's written in Java, download the jar or build yourself and run.

The latest jar is always available [here](http://codeberg.org/glowiak/1D-Blocker/raw/branch/master/app/build/libs/1D-Blocker.jar)

Please note that the game is in very early stage, most things are not done yet.

### Building

This does require 0 external libraries. The only dependency is the Java runtime, version 8 or higher. The jars in releases are always built with java 8.

First, clone this repository:

	git clone http://codeberg.org/1d-blocker.git
	cd 1d-blocker

And start the build:

	./gradlew clean build

This will build a jar in app/build/libs

### Roadmap

[DONE] game working

[DONE] level rendering

[DONE] saving and loading

[DONE] chests

[WIP] tools

[DONE] mobs and monsters

[WIP] other worlds and portals

[DONE] XP system

[WIP] procedurally generated world

[DONE] caves

[WIP] title screen

[DONE] use package; and migrate to gradle

### Screenshots

<p align="center"><img src="http://codeberg.org/glowiak/1d-blocker/raw/branch/master/screenshot.png"></p>

### Credits

My java compiler is OpenJDK, the assets are made using GIMP and Pinta. My IDE is Geany.

## Frequently Asked Questions

### YOU STUPID MORON JAVA IS BLOATED UNSAFE SH\*T DELETE THIS AND REWRITE IN THE MOST BEST LANG RUST

i hate rust and pushing it everywhere so no

### Alpha?

<strike>Yes, alpha is bunch of changes made in one day, the jar is uploaded with a changelog every about 20pm.</strike>

It's not in alpha anymore

### yu ydiot give me the source code

<strike>I will upload sources here, when I get this playable (moving and crafting for example), meanwhile you can decompile the jar to see what's inside AND HACK.</strike> Done.
