package com.glowiak.onedblocker;

import java.io.*;
import java.nio.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import com.glowiak.onedblocker.Elements;
import com.glowiak.onedblocker.Renderer;
import com.glowiak.onedblocker.UpdateMode;
import com.glowiak.onedblocker.Save.SaveGame;
import com.glowiak.onedblocker.Save.LoadGame;
import com.glowiak.onedblocker.UI.Crafting;
import com.glowiak.onedblocker.UI.Chest;

public class Main
{
    public static void main(String[] args)
    {
        Elements el = new Elements();
        Renderer rd = new Renderer();
        UpdateMode um = new UpdateMode();
        SaveGame sg = new SaveGame();
        LoadGame lgu = new LoadGame();
        Crafting craft = new Crafting();
        el.w.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        el.w.setSize(el.WIDTH, el.HEIGHT);
        
        for (int i = 0; i <= 6; i++)
        {
            el.BLOCK[i] = new JButton("");
            el.BLOCK[i].setBorder(null);
        }
        
        el.LeftButton.setBounds(0,520,50,50);
        el.RightButton.setBounds(50,520,50,50);
        el.CraftButton.setBounds(0,470,100,50);
        el.ModeButton.setBounds(811,495,75,75);

        el.PickaxeIcon.setIcon(new ImageIcon(rd.getClass().getResource("/textures/pickaxe.png")));
        el.PickaxeIcon.setBounds(1,1,50,50);
        el.SwordIcon.setIcon(new ImageIcon(rd.getClass().getResource("/textures/sword.png")));
        el.SwordIcon.setBounds(1,51,50,50);
        el.PickLevel.setBounds(59,5,50,50);
        el.SwordLevel.setBounds(59,55,50,50);
        el.XPL.setBounds(820,0,50,50);
        
        el.RSC_WOOD.setBounds(110,490,100,50);
        el.RSC_STONE.setBounds(180,490,100,50);
        el.RSC_IRON.setBounds(250,490,100,50);
        el.RSC_GOLD.setBounds(320,490,100,50);
        el.RSC_DIAMOND.setBounds(390,490,130,50);
        el.RSC_CHEST.setBounds(500,490,100,50);
        el.RSC_DOOR.setBounds(590,490,100,50);
        el.RSC_DIRT.setBounds(680,490,100,50);
        
        el.SaveButt.setBounds(811,250,75,40);
        el.LoadButt.setBounds(811,290,75,40);
        el.SaveButt.addActionListener(sg);
        el.LoadButt.addActionListener(lgu);

        el.ModeButton.addActionListener(um);
        el.CraftButton.addActionListener(craft);

        updateResources();
        updateXpBar();
        updateToolsLevel();
        updateModeButton();
        rd.render();
        
        el.BLOCK[0].setBounds(100,100,el.BLOCK_WIDTH,el.BLOCK_HEIGHT);
        el.BLOCK[1].setBounds(200,100,el.BLOCK_WIDTH,el.BLOCK_HEIGHT);
        el.BLOCK[2].setBounds(300,100,el.BLOCK_WIDTH,el.BLOCK_HEIGHT);
        el.BLOCK[3].setBounds(400,100,el.BLOCK_WIDTH,el.BLOCK_HEIGHT);
        el.BLOCK[4].setBounds(500,100,el.BLOCK_WIDTH,el.BLOCK_HEIGHT);
        el.BLOCK[5].setBounds(600,100,el.BLOCK_WIDTH,el.BLOCK_HEIGHT);
        el.BLOCK[6].setBounds(700,100,el.BLOCK_WIDTH,el.BLOCK_HEIGHT);
        
        el.TEXT_WELCOME.setBounds(300,30,200,30);
        el.SEL_BLOC.setBounds(0,420,300,50);
        el.SEL_WOOD.setText("SEL");
        el.SEL_STONE.setText("SEL");
        el.SEL_IRON.setText("SEL");
        el.SEL_GOLD.setText("SEL");
        el.SEL_DIAMOND.setText("SEL");
        el.SEL_CHEST.setText("SEL");
        el.SEL_DOOR.setText("SEL");
        el.SEL_DIRT.setText("SEL");

        el.SEL_WOOD.setBounds(110,540,60,30);
        el.SEL_STONE.setBounds(180,540,60,30);
        el.SEL_IRON.setBounds(250,540,60,30);
        el.SEL_GOLD.setBounds(320,540,60,30);
        el.SEL_DIAMOND.setBounds(390,540,60,30);
        el.SEL_CHEST.setBounds(500,540,60,30);
        el.SEL_DOOR.setBounds(590,540,60,30);
        el.SEL_DIRT.setBounds(680,540,60,30);
        
        el.HEART.setBounds(0,410,150,30);
        
        el.SEL_WOOD.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e) {
                Elements.SELECTED_BLOCK = '4';
                updateResources();
            }
        });
        el.SEL_STONE.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e) {
                Elements.SELECTED_BLOCK = '3';
                updateResources();
            }
        });
        el.SEL_DIRT.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e) {
                Elements.SELECTED_BLOCK = '2';
                updateResources();
            }
        });
        el.SEL_CHEST.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e) {
                Elements.SELECTED_BLOCK = '7';
                updateResources();
            }
        });
        el.SEL_DOOR.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e) {
                Elements.SELECTED_BLOCK = '8';
                updateResources();
            }
        });
        el.SEL_IRON.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e) {
                Elements.SELECTED_BLOCK = '5';
                updateResources();
            }
        });
        el.SEL_GOLD.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e) {
                Elements.SELECTED_BLOCK = '6';
                updateResources();
            }
        });
        el.SEL_DIAMOND.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e) {
                Elements.SELECTED_BLOCK = 'd';
                updateResources();
            }
        });
        
        el.LeftButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent ep)
            {
                if (Elements.MAP[Elements.DIMENSION][Elements.PLAYER_POS - 1] == 'C')
                {
                    if (Elements.DIMENSION == 0) {
                        Elements.DIMENSION = 1;
                    } else if (Elements.DIMENSION == 1) {
                        Elements.DIMENSION = 0;
                    }
                    Renderer.render();
                }
                if (Tile.getTileFromId(Elements.MAP[Elements.DIMENSION][Elements.PLAYER_POS - 1]) == "air" || Tile.getTileFromId(Elements.MAP[Elements.DIMENSION][Elements.PLAYER_POS - 1]) == "chest" || Tile.getTileFromId(Elements.MAP[Elements.DIMENSION][Elements.PLAYER_POS - 1]) == "door")
                {
                    Elements.PLAYER_POS--;
                    Renderer.render();
                } // only move if it's not solid block
            }
        });
        el.RightButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent emp)
            {
                if (Tile.getTileFromId(Elements.MAP[Elements.DIMENSION][Elements.PLAYER_POS + 1]) == "air" || Tile.getTileFromId(Elements.MAP[Elements.DIMENSION][Elements.PLAYER_POS + 1]) == "chest" || Tile.getTileFromId(Elements.MAP[Elements.DIMENSION][Elements.PLAYER_POS + 1]) == "door")
                {
                    Elements.PLAYER_POS++;
                    Renderer.render();
                } // only move if it's not solid block
            }
        });
        
        el.BLOCK[2].addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent aeact)
            {
                if (Elements.MAP[Elements.DIMENSION][Elements.PLAYER_POS - 1] != '0' && Elements.MAP[Elements.DIMENSION][Elements.PLAYER_POS - 1] != 'z' && Elements.MAP[Elements.DIMENSION][Elements.PLAYER_POS - 1] != 'p' && Elements.MAP[Elements.DIMENSION][Elements.PLAYER_POS - 1] != 'C' && Elements.state == 0)
                {
                    if (Tile.getHarvestLevel(Elements.MAP[Elements.DIMENSION][Elements.PLAYER_POS - 1]) >= Elements.TOOLS_LEVEL[0])
                    { // don't continue if level is too low
                        char cId = Elements.MAP[Elements.DIMENSION][Elements.PLAYER_POS - 1];
                        if (cId == '1' || cId == '2') { // dirt and grass
                            Elements.Inventory[0]++;
                        }
                        if (cId == '3') { // stone
                            Elements.Inventory[1]++;
                        }
                        if (cId == '4') { // wood
                            Elements.Inventory[2] = Elements.Inventory[2] + 4; // 1 wood = 4 planks right
                        }
                        if (cId == '5') { // iron ore
                            Elements.Inventory[5]++;
                        }
                        if (cId == '6') { // gold ore
                            Elements.Inventory[6]++;
                        }
                        if (cId == '7') { // chest
                            Elements.Inventory[7]++;
                        }
                        if (cId == 'd') { // diamond
                            Elements.Inventory[3]++;
                        }
                        Elements.MAP[Elements.DIMENSION][Elements.PLAYER_POS - 1] = '0'; // set the block to air
                        Renderer.render(); // re-render the terrain
                        updateResources(); // refresh resource labels
                    }
                }
                if (Tile.getTileFromId(Elements.MAP[Elements.DIMENSION][Elements.PLAYER_POS - 1]) == "air" && Elements.state == 1)
                {
                    char cId = Elements.SELECTED_BLOCK;
                    int index = 0;
                    if (cId == '1' || cId == '2') { // dirt
                        index = 0;
                    }
                    if (cId == '3') { // stone
                        index = 1;
                    }
                    if (cId == '4') { // wood
                        index = 2;
                    }
                    if (cId == 'd') { // diamond ore
                        index = 3;
                    }
                    if (cId == '5') { // iron ore
                        index = 5;
                    }
                    if (cId == '6') { // gold ore
                        index = 6;
                    }
                    if (cId == '7') { // chest
                        index = 7;
                    }
                    if (cId == '8') { // door
                        index = 8;
                    }
                    if (Elements.Inventory[index] > 0) {
                        Elements.Inventory[index]--;
                        Elements.MAP[Elements.DIMENSION][Elements.PLAYER_POS - 1] = cId;
                        Renderer.render();
                        updateResources();
                    }
                }
                if (Tile.getTileFromId(Elements.MAP[Elements.DIMENSION][Elements.PLAYER_POS - 1]) == "chest" && Elements.state == 3)
                {
                    Chest.openChest();
                }
                if (Elements.MAP[Elements.DIMENSION][Elements.PLAYER_POS - 1] == 'z' && Elements.state == 0)
                {
                    if (Elements.monster.getHealth() <= 1) {
                        Elements.MAP[Elements.DIMENSION][Elements.PLAYER_POS - 1] = '0';
                        Elements.Inventory[5]++;
                        
                        Elements.monster.setHealth(5);
                        Elements.XP = Elements.XP + Elements.monster.getDropXp();
                        
                        Renderer.render();
                        updateResources();
                        updateXpBar();
                    } else {
                        Elements.monster.clicked();
                    }
                }
                if (Elements.MAP[Elements.DIMENSION][Elements.PLAYER_POS - 1] == 'p' && Elements.state == 0)
                {
                    if (Elements.pig.getHealth() <= 1) {
                        Elements.MAP[Elements.DIMENSION][Elements.PLAYER_POS - 1] = '0';
                        Elements.HEALTH = Elements.HEALTH + 3;
                        
                        Elements.pig.setHealth(2);
                        Elements.XP = Elements.XP + Elements.pig.getDropXp();
                        
                        Renderer.render();
                        updateResources();
                        updateXpBar();
                    } else {
                        Elements.pig.clicked();
                    }
                }
            }
        });
        el.BLOCK[4].addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent aeactt)
            {
                if (Elements.MAP[Elements.DIMENSION][Elements.PLAYER_POS + 1] != '0' && Elements.MAP[Elements.DIMENSION][Elements.PLAYER_POS + 1] != 'z' && Elements.MAP[Elements.DIMENSION][Elements.PLAYER_POS + 1] != 'C' && Elements.MAP[Elements.DIMENSION][Elements.PLAYER_POS + 1] != 'p' && Elements.state == 0)
                {
                    if (Tile.getHarvestLevel(Elements.MAP[Elements.DIMENSION][Elements.PLAYER_POS + 1]) >= Elements.TOOLS_LEVEL[0])
                    { // don't continue if level is too low
                        int index = 0;
                        char cId = Elements.MAP[Elements.DIMENSION][Elements.PLAYER_POS + 1];
                        if (cId == '1' || cId == '2') { // dirt and grass
                            Elements.Inventory[0]++;
                        }
                        if (cId == '3') { // stone
                            Elements.Inventory[1]++;
                        }
                        if (cId == '4') { // wood
                            Elements.Inventory[2] = Elements.Inventory[2] + 4; // 1 wood = 4 planks right
                        }
                        if (cId == '5') { // iron ore
                            Elements.Inventory[5]++;
                        }
                        if (cId == '6') { // gold ore
                            Elements.Inventory[6]++;
                        }
                        if (cId == '7') { // chest
                            Elements.Inventory[7]++;
                        }
                        if (cId == 'd') { // diamond
                            Elements.Inventory[3]++;
                        }
                        Elements.MAP[Elements.DIMENSION][Elements.PLAYER_POS + 1] = '0'; // set the block to air
                        Renderer.render(); // re-render the terrain
                        updateResources(); // refresh resource labels
                    }
                }
                if (Tile.getTileFromId(Elements.MAP[Elements.DIMENSION][Elements.PLAYER_POS + 1]) == "air" && Elements.state == 1)
                {
                    char cId = Elements.SELECTED_BLOCK;
                    int index = 0;
                    if (cId == '1' || cId == '2') { // dirt
                        index = 0;
                    }
                    if (cId == '3') { // stone
                        index = 1;
                    }
                    if (cId == '4') { // wood
                        index = 2;
                    }
                    if (cId == 'd') { // diamond ore
                        index = 3;
                    }
                    if (cId == '5') { // iron ore
                        index = 5;
                    }
                    if (cId == '6') { // gold ore
                        index = 6;
                    }
                    if (cId == '7') { // chest
                        index = 7;
                    }
                    if (cId == '8') { // door
                        index = 8;
                    }
                    if (Elements.Inventory[index] > 0) {
                        Elements.Inventory[index]--;
                        Elements.MAP[Elements.DIMENSION][Elements.PLAYER_POS + 1] = cId;
                        Renderer.render();
                        updateResources();
                    }
                }
                if (Tile.getTileFromId(Elements.MAP[Elements.DIMENSION][Elements.PLAYER_POS + 1]) == "chest" && Elements.state == 3)
                {
                    Chest.openChest();
                }
                if (Elements.MAP[Elements.DIMENSION][Elements.PLAYER_POS + 1] == 'z' && Elements.state == 0)
                {
                    if (Elements.monster.getHealth() <= 1) {
                        Elements.MAP[Elements.DIMENSION][Elements.PLAYER_POS + 1] = '0';
                        Elements.Inventory[5]++;
                        
                        Elements.monster.setHealth(5);
                        Elements.XP = Elements.XP + Elements.monster.getDropXp();
                        
                        Renderer.render();
                        updateResources();
                        updateXpBar();
                    } else {
                        Elements.monster.clicked();
                    }
                }
                if (Elements.MAP[Elements.DIMENSION][Elements.PLAYER_POS + 1] == 'p' && Elements.state == 0)
                {
                    if (Elements.pig.getHealth() <= 1) {
                        Elements.MAP[Elements.DIMENSION][Elements.PLAYER_POS + 1] = '0';
                        Elements.HEALTH = Elements.HEALTH + 3;
                        
                        Elements.pig.setHealth(2);
                        Elements.XP = Elements.XP + Elements.pig.getDropXp();
                        
                        Renderer.render();
                        updateResources();
                        updateXpBar();
                    } else {
                        Elements.pig.clicked();
                    }
                }
            }
        });
        
        el.w.add(el.BLOCK[0]);
        el.w.add(el.BLOCK[1]);
        el.w.add(el.BLOCK[2]);
        el.w.add(el.BLOCK[3]);
        el.w.add(el.BLOCK[4]);
        el.w.add(el.BLOCK[5]);
        el.w.add(el.BLOCK[6]);
        el.w.add(el.TEXT_WELCOME);
        el.w.add(el.LeftButton);
        el.w.add(el.RightButton);
        el.w.add(el.CraftButton);
        el.w.add(el.ModeButton);
        el.w.add(el.PickaxeIcon);
        el.w.add(el.SwordIcon);
        el.w.add(el.PickLevel);
        el.w.add(el.SwordLevel);
        el.w.add(el.XPL);
        el.w.add(el.RSC_WOOD);
        el.w.add(el.RSC_STONE);
        el.w.add(el.RSC_IRON);
        el.w.add(el.RSC_GOLD);
        el.w.add(el.RSC_DIAMOND);
        el.w.add(el.RSC_CHEST);
        el.w.add(el.RSC_DOOR);
        el.w.add(el.SaveButt);
        el.w.add(el.LoadButt);
        el.w.add(el.RSC_DIRT);
        el.w.add(el.SEL_BLOC);
        el.w.add(el.SEL_WOOD);
        el.w.add(el.SEL_STONE);
        el.w.add(el.SEL_IRON);
        el.w.add(el.SEL_GOLD);
        el.w.add(el.SEL_DIAMOND);
        el.w.add(el.SEL_CHEST);
        el.w.add(el.SEL_DOOR);
        el.w.add(el.SEL_DIRT);
        el.w.add(el.HEART);
        
        el.w.setResizable(false);
        el.w.setLayout(null);
        el.w.setVisible(true);
    }
    public static void updateModeButton()
    {
        Elements.ModeButton.setText(Tile.getTaskNameFromId(Elements.state));
    }
    public static void updateToolsLevel()
    {
        Elements.PickLevel.setText(String.format("LVL %d", Elements.TOOLS_LEVEL[0]));
        Elements.SwordLevel.setText(String.format("LVL %d", Elements.TOOLS_LEVEL[1]));
    }
    public static void updateXpBar()
    {
        Elements.XPL.setText(String.format("XP: %d", Elements.XP));
    }
    public static void updateResources()
    {
        if (Elements.HEALTH == 0)
        {
            JOptionPane.showMessageDialog(Elements.w, "You died.");
            System.exit(0);
        }
        Elements.RSC_DIRT.setText(String.format("Dirt: %d", Elements.Inventory[0]));
        Elements.RSC_WOOD.setText(String.format("Wood: %d", Elements.Inventory[2]));
        Elements.RSC_STONE.setText(String.format("Stone: %d", Elements.Inventory[1]));
        Elements.RSC_IRON.setText(String.format("Iron: %d", Elements.Inventory[5]));
        Elements.RSC_GOLD.setText(String.format("Gold: %d", Elements.Inventory[6]));
        Elements.RSC_DIAMOND.setText(String.format("Diamonds: %d",Elements.Inventory[3]));
        Elements.RSC_CHEST.setText(String.format("Chests: %d", Elements.Inventory[7]));
        Elements.RSC_DOOR.setText(String.format("Doors: %d",Elements.Inventory[8]));
        
        Elements.SEL_BLOC.setText(String.format("Selected: %s", Tile.getItemTile(Elements.SELECTED_BLOCK)));
        Elements.HEART.setText(String.format("Health: %d", Elements.HEALTH));
    }
}
