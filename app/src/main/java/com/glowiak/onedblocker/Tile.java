package com.glowiak.onedblocker;

/* The Tile and Item class
 * 
 * The tile mapping:
 * 0 - air
 * 1 - grass
 * 2 - dirt
 * 3 - stone
 * 4 - wood
 * 5 - cobblestone
 * 6 - chest*
 * 7 - door*
 * 
 *  * - can be pass-throu
 * 
 * The item mapping:
 * 0 - dirt
 * 1 - dirt
 * 2 - stone
 * 3 - wood
 * 4 - iron
 * 5 - gold
 * 6 - diamonds
 * 7 - cobblestone
 * 8 - chest
 * 9 - door
 * 
 * Task ID mapping:
 * 0 - break
 * 1 - place
 * 2 - fight
 * 3 - open
 * 
 * Hard levels:
 * 0 - dirt, grass, wood, chest, door
 * 1 - stone, cobblestone
 * 2 - ores
 */

public class Tile
{
    public static String getItemTile(char id)
    {
        String nam = "";
        switch(id) {
            case '1':
                nam = "dirt";
                break;
            case '2':
                nam = "dirt";
                break;
            case '3':
                nam = "stone";
                break;
            case '4':
                nam = "wood";
                break;
            case '5':
                nam = "iron";
                break;
            case '6':
                nam = "gold";
                break;
            case '7':
                nam = "chest";
                break;
            case '8':
                nam = "door";
                break;
            case 'd':
                nam = "diamond";
                break;
        }
        return nam;
    }
    public static char getIdFromTile(String name)
    {
        String nam = name.toLowerCase();
        char namId = '0';
        if (nam == "air")
        {
            namId = '0';
        }
        if (nam == "grass")
        {
            namId = '1';
        }
        if (nam == "dirt")
        {
            namId = '2';
        }
        if (nam == "stone")
        {
            namId = '3';
        }
        if (nam == "wood")
        {
            namId = '4';
        }
        if (nam == "iron")
        {
            namId = '5';
        }
        if (nam == "gold")
        {
            namId = '6';
        }
        if (nam == "chest")
        {
            namId = '7';
        }
        if (nam == "door")
        {
            namId = '8';
        }
        if (nam == "diamond")
        {
            namId = 'd';
        }
        
        return namId;
    }
    public static String getTileFromId(char id)
    {
        String idNam = "";
        if (id == '0')
        {
            idNam = "air";
        }
        if (id == '1')
        {
            idNam = "grass";
        }
        if (id == '2')
        {
            idNam = "dirt";
        }
        if (id == '3')
        {
            idNam = "stone";
        }
        if (id == '4')
        {
            idNam = "wood";
        }
        if (id == '5')
        {
            idNam = "iron";
        }
        if (id == '6')
        {
            idNam = "gold";
        }
        if (id == '8')
        {
            idNam = "door";
        }
        if (id == '7')
        {
            idNam = "chest";
        }
        if (id == 'd')
        {
            idNam = "diamond";
        }
        
        return idNam;
    }
    public static String getTaskNameFromId(int id)
    {
        String vtr = "";
        if (id == 0)
        {
            vtr = "break";
        }
        if (id == 1)
        {
            vtr = "place";
        }
        if (id == 2)
        {
            vtr = "fight";
        }
        if (id == 3)
        {
            vtr = "open";
        }
        return vtr;
    }
    public static int getHarvestLevel(char id)
    {
        int vtr = 0;
        if (id == '1' || id == '2' || id == '4' || id == '6' || id == '7')
        {
            vtr = 0;
        }
        if (id == '3' || id == '5')
        {
            vtr = 1;
        }
        
        return vtr;
    }
}
