package com.glowiak.onedblocker;

import javax.swing.ImageIcon;
import com.glowiak.onedblocker.Renderer;
import com.glowiak.onedblocker.Elements;

// the renderer class

public class Renderer
{
    public static void render()
    {
        Renderer obj = new Renderer();
        for (int i = 0; i <= 6; i++)
        {
            // get element id
            int linePos = Elements.PLAYER_POS + Elements.diff[i];
            
            char c = Elements.MAP[Elements.DIMENSION][linePos];
            String blockName = Tile.getTileFromId(c);
            
            if (i == 3)
            {
                blockName = "player"; // player is always on the third block
            }
            if (c == 'z') // the monster
            {
                blockName = "zombie";
            }
            if (c == 'p') // the pig
            {
                blockName = "pig";
            }
            if (c == 'C') // the cave entrance
            {
                blockName = "entrance";
            }
            
            Elements.BLOCK[i].setIcon(new ImageIcon(obj.getClass().getResource(String.format("/textures/%s.png", blockName))));
        }
    }
}
