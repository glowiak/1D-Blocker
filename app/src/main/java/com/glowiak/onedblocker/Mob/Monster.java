package com.glowiak.onedblocker.Mob;

import com.glowiak.onedblocker.Elements;
import com.glowiak.onedblocker.Main;

public class Monster
{
    private String Tname = "zombie";
    private int Thealth = 5;
    private int Tdamage = 1;
    private int dropXp = 3;
    
    public Monster(String name, int health, int damage, int xp)
    {
        initMonster(name, health, damage, xp);
    }
    
    public void clicked()
    {
        Thealth = Thealth - Elements.TOOLS_LEVEL[1] - 1; // you attack him
        Elements.HEALTH = Elements.HEALTH - Tdamage; // he in turn attacks you
        
        Main.updateResources(); // update the bars
    }
    public String getName()
    {
        return Tname;
    }
    public int getHealth()
    {
        return Thealth;
    }
    public int getDamage()
    {
        return Tdamage;
    }
    public void setHealth(int Nhealth)
    {
        Thealth = Nhealth;
    }
    public int getDropXp()
    {
        return dropXp;
    }
    public void initMonster(String name, int health, int damage, int xp)
    {
        Tname = name;
        Thealth = health;
        Tdamage = damage;
        dropXp = xp;
    }
}
