package com.glowiak.onedblocker;

import java.awt.*;
import java.awt.event.*;
import com.glowiak.onedblocker.Elements;
import com.glowiak.onedblocker.Main;

public class UpdateMode implements ActionListener
{
    public void actionPerformed(ActionEvent e)
    {
        if (Elements.state < 3) {
            Elements.state++;
        } else {
            Elements.state = 0;
        }
        Main.updateModeButton();
    }
}
