package com.glowiak.onedblocker.UI;

import java.io.*;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.*;
import java.awt.event.*;
import com.glowiak.onedblocker.Elements;
import com.glowiak.onedblocker.Main;

public class Crafting implements ActionListener
{
    public void actionPerformed(ActionEvent e)
    {
        JFrame CW = new JFrame("Craft");
        CW.setSize(400,500);
        
        JLabel TEXT_CHEST = new JLabel("Chest (8W)");
        TEXT_CHEST.setBounds(10,10,80,50);
        
        JButton CRAFT_CHEST = new JButton("Craft");
        CRAFT_CHEST.setBounds(315,22,70,30);
        CRAFT_CHEST.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent ee)
            {
                if (Elements.Inventory[2] >= 8)
                {
                    Elements.Inventory[7]++; // add the item
                    Elements.Inventory[2] = Elements.Inventory[2] - 8; // remove the items
                    
                    Main.updateResources(); // refresh labels
                }
            }
        });
        
        JLabel TEXT_DOOR = new JLabel("Door (6W)");
        TEXT_DOOR.setBounds(10,60,80,30);
        
        JButton CRAFT_DOOR = new JButton("Craft");
        CRAFT_DOOR.setBounds(315,62,70,30);
        CRAFT_DOOR.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                if (Elements.Inventory[2] >= 6)
                {
                    Elements.Inventory[8]++;
                    Elements.Inventory[2] = Elements.Inventory[2] - 6;
                    
                    Main.updateResources();
                }
            }
        });
        
        JLabel TEXT_SWUPG = new JLabel("Sword LVL (2 IRON)");
        TEXT_SWUPG.setBounds(10,110,150,30);
        
        JButton CRAFT_SWUPG = new JButton("Craft");
        CRAFT_SWUPG.setBounds(315,102,70,30);
        CRAFT_SWUPG.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                if (Elements.Inventory[5] >= 2)
                {
                    Elements.TOOLS_LEVEL[1]++;
                    Elements.Inventory[5] = Elements.Inventory[5] - 2;
                    
                    Main.updateToolsLevel();
                    Main.updateResources();
                }
            }
        });
        
        CW.add(TEXT_CHEST);
        CW.add(CRAFT_CHEST);
        CW.add(TEXT_DOOR);
        CW.add(CRAFT_DOOR);
        CW.add(TEXT_SWUPG);
        CW.add(CRAFT_SWUPG);
        
        CW.setLayout(null);
        CW.setVisible(true);
    }
}
