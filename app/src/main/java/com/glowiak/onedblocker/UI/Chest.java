package com.glowiak.onedblocker.UI;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.*;
import java.awt.event.*;
import com.glowiak.onedblocker.Elements;
import com.glowiak.onedblocker.Main;

interface ChestBlocks {
    JFrame cw = new JFrame("Chest");
    JLabel T_WOOD = new JLabel();
    JLabel T_STONE = new JLabel();
    JLabel T_IRON = new JLabel();
    JLabel T_GOLD = new JLabel();
    JLabel T_DIAMOND = new JLabel();
    JLabel T_DIRT = new JLabel();
    JLabel T_CHEST = new JLabel();
    JLabel T_DOOR = new JLabel();
    JButton BUTT_TOINV = new JButton("To inv");
    JButton BUTT_TOCHEST = new JButton("To chest");
}

public class Chest implements ChestBlocks
{
    public static void openChest()
    {
        cw.setSize(400,500);
        
        T_WOOD.setBounds(1,1,150,30);
        T_STONE.setBounds(1,30,150,30);
        T_IRON.setBounds(1,60,150,30);
        T_GOLD.setBounds(1,90,150,30);
        T_DIAMOND.setBounds(1,120,150,30);
        T_DIRT.setBounds(1,150,150,30);
        T_CHEST.setBounds(1,180,150,30);
        T_DOOR.setBounds(1,210,150,30);
        
        BUTT_TOINV.setBounds(0,430,75,40);
        BUTT_TOCHEST.setBounds(291,430,95,40);
        BUTT_TOINV.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e) {
                Elements.Inventory[0] = Elements.Inventory[0] + Elements.Chest[0];
                Elements.Chest[0] = 0;
                
                Elements.Inventory[1] = Elements.Inventory[1] + Elements.Chest[1];
                Elements.Chest[1] = 0;
                
                Elements.Inventory[2] = Elements.Inventory[2] + Elements.Chest[2];
                Elements.Chest[2] = 0;
                
                Elements.Inventory[3] = Elements.Inventory[3] + Elements.Chest[3];
                Elements.Chest[3] = 0;
                
                Elements.Inventory[5] = Elements.Inventory[5] + Elements.Chest[5];
                Elements.Chest[5] = 0;

                Elements.Inventory[6] = Elements.Inventory[6] + Elements.Chest[6];
                Elements.Chest[6] = 0;
                
                Elements.Inventory[7] = Elements.Inventory[7] + Elements.Chest[7];
                Elements.Chest[7] = 0;
                
                Elements.Inventory[8] = Elements.Inventory[8] + Elements.Chest[8];
                Elements.Chest[8] = 0;
                
                updateChest();
                Main.updateResources();
            }
        });
        BUTT_TOCHEST.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                Elements.Chest[0] = Elements.Chest[0] + Elements.Inventory[0];
                Elements.Inventory[0] = 0;
                
                Elements.Chest[1] = Elements.Chest[1] + Elements.Inventory[1];
                Elements.Inventory[1] = 0;
                
                Elements.Chest[2] = Elements.Chest[2] + Elements.Inventory[2];
                Elements.Inventory[2] = 0;
                
                Elements.Chest[3] = Elements.Chest[3] + Elements.Inventory[3];
                Elements.Inventory[3] = 0;
                
                Elements.Chest[5] = Elements.Chest[5] + Elements.Inventory[5];
                Elements.Inventory[5] = 0;

                Elements.Chest[6] = Elements.Chest[6] + Elements.Inventory[6];
                Elements.Inventory[6] = 0;
                
                Elements.Chest[7] = Elements.Chest[7] + Elements.Inventory[7];
                Elements.Inventory[7] = 0;
                
                Elements.Chest[8] = Elements.Chest[8] + Elements.Inventory[8];
                Elements.Inventory[8] = 0;
                
                updateChest();
                Main.updateResources();
            }
        });
        
        updateChest();
        
        cw.add(T_WOOD);
        cw.add(T_STONE);
        cw.add(T_IRON);
        cw.add(T_GOLD);
        cw.add(T_DIAMOND);
        cw.add(T_DIRT);
        cw.add(T_CHEST);
        cw.add(T_DOOR);
        
        cw.add(BUTT_TOINV);
        cw.add(BUTT_TOCHEST);
        
        cw.setLayout(null);
        cw.setVisible(true);
    }
    public static void updateChest()
    {
        Chest.T_WOOD.setText(String.format("Wood: %d",Elements.Chest[2]));
        Chest.T_STONE.setText(String.format("Stone: %d",Elements.Chest[1]));
        Chest.T_IRON.setText(String.format("Iron: %d",Elements.Chest[5]));
        Chest.T_GOLD.setText(String.format("Gold: %d",Elements.Chest[6]));
        Chest.T_DIRT.setText(String.format("Dirt: %d",Elements.Chest[0]));
        Chest.T_STONE.setText(String.format("Stone: %d",Elements.Chest[1]));
        Chest.T_CHEST.setText(String.format("Chests: %d",Elements.Chest[7]));
        Chest.T_DOOR.setText(String.format("Doors: %d",Elements.Chest[8]));
        Chest.T_DIAMOND.setText(String.format("Diamonds: %d",Elements.Chest[3]));
    }
}
