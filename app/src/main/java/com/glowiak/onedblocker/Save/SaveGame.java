package com.glowiak.onedblocker.Save;

import java.io.*;
import javax.swing.JFileChooser;
import java.awt.*;
import java.awt.event.*;
import com.glowiak.onedblocker.Elements;

public class SaveGame implements ActionListener
{
    public void actionPerformed(ActionEvent eee)
    {
        try {
            doSave();
        } catch(IOException ioe) {
            System.out.println(ioe);
            System.exit(1);
        }
    }
    public void doSave() throws IOException
    {
        JFileChooser cf = new JFileChooser();
        int od = cf.showSaveDialog(Elements.w);
        if (od == JFileChooser.APPROVE_OPTION)
        {
            if (cf.getSelectedFile().exists())
            {
                File rmf = new File(cf.getSelectedFile().getPath());
                rmf.delete();
            }
            System.out.println(cf.getSelectedFile().getPath());
            FileWriter but = new FileWriter(cf.getSelectedFile().getPath());
            for (int m = 0; m < Elements.MAP[0].length; m++)
            {
                but.write(String.format("%c\n", Elements.MAP[0][m])); // save the surface
            }
            for (int n = 0; n < Elements.MAP[1].length; n++)
            {
                but.write(String.format("%c\n", Elements.MAP[1][n])); // save the cave
            }
            but.write(String.format("%d\n",Elements.PLAYER_POS));
            for (int i = 0; i < 9; i++)
            {
                but.write(String.format("%d\n",Elements.Inventory[i]));
            }
            for (int j = 0; j < 2; j++)
            {
                but.write(String.format("%d\n",Elements.TOOLS_LEVEL[j]));
            }
            for (int k = 0; k < 9; k++)
            {
                but.write(String.format("%d\n",Elements.Chest[k]));
            }
            but.write(String.format("%d\n",Elements.HEALTH));
            but.write(String.format("%d\n",Elements.XP));
            but.write(String.format("%d\n", Elements.DIMENSION));
            but.close();
        }
    }
}
