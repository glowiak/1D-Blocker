package com.glowiak.onedblocker.Save;

import java.io.*;
import javax.swing.JFileChooser;
import java.awt.*;
import java.awt.event.*;
import com.glowiak.onedblocker.Elements;
import com.glowiak.onedblocker.Main;
import com.glowiak.onedblocker.Renderer;

public class LoadGame implements ActionListener
{
    public void actionPerformed(ActionEvent actt)
    {
        try {
            doLoad();
        } catch(IOException ioe) {
            System.out.println(ioe);
            System.exit(1);
        }
    }
    public void doLoad() throws IOException
    {
        JFileChooser fc = new JFileChooser(); // choose the .mll file
        int oc = fc.showOpenDialog(Elements.w);
        if (oc == JFileChooser.APPROVE_OPTION)
        {
            FileReader fr = new FileReader(fc.getSelectedFile().getPath());
            BufferedReader br = new BufferedReader(fr);
            for (int m = 0; m < Elements.MAP[0].length; m++)
            {
                Elements.MAP[0][m] = br.readLine().charAt(0); // load the surface
            }
            for (int n = 0; n < Elements.MAP[1].length; n++)
            {
                Elements.MAP[1][n] = br.readLine().charAt(0); // load the caves
            }
            Elements.PLAYER_POS = Integer.parseInt(br.readLine());
            for (int i = 0; i < 9; i++)
            {
                Elements.Inventory[i] = Integer.parseInt(br.readLine());
            }
            for (int j = 0; j < 2; j++)
            {
                Elements.TOOLS_LEVEL[j] = Integer.parseInt(br.readLine());
            }
            for (int k = 0; k < 9; k++)
            {
                Elements.Chest[k] = Integer.parseInt(br.readLine());
            }
            Elements.HEALTH = Integer.parseInt(br.readLine());
            Elements.XP = Integer.parseInt(br.readLine());
            Elements.DIMENSION = Integer.parseInt(br.readLine());
            Main.updateResources();
            Main.updateToolsLevel();
            Renderer.render();
        }
    }
}
