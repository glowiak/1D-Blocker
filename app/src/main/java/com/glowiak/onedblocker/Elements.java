package com.glowiak.onedblocker;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import com.glowiak.onedblocker.Mob.Monster;

/* This is class for the definitions.
 * No actual code is here.
 */


/* Dimensions:
 * 0 - overworld
 * 1 - cave
 */
 
public class Elements
{
    public static final String APP_NAME = "1D Blocker";
    public static final double APP_VERSION = 1.9;
    
    public static int XP = 0;
    public static int WOOD_COUNT = 0;
    public static int STONE_COUNT = 0;
    public static int IRON_COUNT = 0;
    public static int GOLD_COUNT = 0;
    public static int DIAMOND_COUNT = 0;
    public static int CHEST_COUNT = 0;
    
    public static int DIMENSION = 0;
    public static int PLAYER_POS = 10;
    public static char[][] MAP = {{ '3','3','C', 'd','6','5','5','3','2','1','0','0','4','z','4','4','p','0','3','3','d','d','3','3','0','0','0','0','0','0','0','0','0','0','0','0' },
        { '3', '3', 'C', '0', '0', '3', '3', '3', 'z', 'z', 'z', '3', '3', '3', '5', '5', '5', '5', '3', '3', '3', '3', '6', '6', '5', 'd', 'd' }};
    
    public static int HEALTH = 20;
    public static Monster monster = new Monster("zombie", 5, 1, 3);
    public static Monster pig = new Monster("pig", 2, 0, 1);
    
    public static JFrame w = new JFrame(APP_NAME);
    public static final int WIDTH = 900;
    public static final int HEIGHT = 600;
    
    public static JButton[] BLOCK = new JButton[7];
    
    public static JLabel TEXT_WELCOME = new JLabel("Welcome to 1D Blocker!");
    
    public static final int BLOCK_WIDTH = 100;
    public static final int BLOCK_HEIGHT = 100;
    
    public static int[] diff = {-3, -2, -1, 0, 1, 2, 3};
    public static int state = 0;
    public static int[] Inventory = { 0,0,0,0,0,0,0,0,0 };
    public static int[] TOOLS_LEVEL = { 0,0 };
    public static char SELECTED_BLOCK = '2'; // dirt by default
    public static int[] Chest = { 0,0,0,0,0,0,0,0,0 };

    public static JButton LeftButton = new JButton("<");
    public static JButton RightButton = new JButton(">");
    public static JButton CraftButton = new JButton("Craft");
    public static JButton ModeButton = new JButton("");
    
    public static JLabel PickaxeIcon = new JLabel("");
    public static JLabel SwordIcon = new JLabel("");
    
    public static JLabel PickLevel = new JLabel();
    public static JLabel SwordLevel = new JLabel();
    public static JLabel XPL = new JLabel();
    
    public static JLabel RSC_WOOD = new JLabel();
    public static JLabel RSC_STONE = new JLabel();
    public static JLabel RSC_IRON = new JLabel();
    public static JLabel RSC_GOLD = new JLabel();
    public static JLabel RSC_DIAMOND = new JLabel();
    public static JLabel RSC_CHEST = new JLabel();
    public static JLabel RSC_DOOR = new JLabel();
    public static JLabel RSC_DIRT = new JLabel();
    
    public static JButton SEL_WOOD = new JButton();
    public static JButton SEL_STONE = new JButton();
    public static JButton SEL_IRON = new JButton();
    public static JButton SEL_GOLD = new JButton();
    public static JButton SEL_DIAMOND = new JButton();
    public static JButton SEL_CHEST = new JButton();
    public static JButton SEL_DOOR = new JButton();
    public static JButton SEL_DIRT = new JButton();
    
    public static JButton SaveButt = new JButton("Save");
    public static JButton LoadButt = new JButton("Load");
    public static JLabel SEL_BLOC = new JLabel();
    public static JLabel HEART = new JLabel();
}
